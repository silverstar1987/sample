<?php
function highestProductOf3($arrayOfInts)
{
    //Check $nValues length
    if(count($arrayOfInts) < 3)
        echo "Exception";

    // We're going to start at the 3rd item (at index 2)
    // so pre-populate highests and lowests based on the first 2 items.
    // we could also start these as null and check below if they're set
    // but this is arguably cleaner
    $highest = max($arrayOfInts[0], $arrayOfInts[1]);
    $lowest  = min($arrayOfInts[0], $arrayOfInts[1]);

    $highestProductOf2 = $arrayOfInts[0] * $arrayOfInts[1];
    $lowestProductOf2  = $arrayOfInts[0] * $arrayOfInts[1];

    // except this one--we pre-populate it for the first /3/ items.
    // this means in our first pass it'll check against itself, which is fine.

    $highestProductOf3 = 1;
    for ($i = 0; $i < count($arrayOfInts); $i++) {
        $highestProductOf3 = $highestProductOf3 * $arrayOfInts[$i];
    }

    // walk through items, starting at index 2
    for ($i = 2; $i < count($arrayOfInts); $i++) {
        $current = $arrayOfInts[$i];

        // do we have a new highest product of 3?
        // it's either the current highest,
        // or the current times the highest product of two
        // or the current times the lowest product of two
        $highestProductOf3 = max(
        $highestProductOf3,
        $current * $highestProductOf2,
        $current * $lowestProductOf2
        );

        // do we have a new highest product of two?
        $highestProductOf2 = max(
        $highestProductOf2,
        $current * $highest,
        $current * $lowest
        );

        // do we have a new lowest product of two?
        $lowestProductOf2 = min(
        $lowestProductOf2,
        $current * $highest,
        $current * $lowest
        );

        // do we have a new highest?
        $highest = max($highest, $current);

        // do we have a new lowest?
        $lowest = min($lowest, $current);
    }

    return $highestProductOf3;
}
?>
